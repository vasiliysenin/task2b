import express from 'express';
import cors from 'cors';

// https://git-scm.com/book/ru/v1/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D1%8C-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9-%D0%B2-%D1%80%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B9
// https://git-scm.com/book/ru/v1/Основы-Git-Запись-изменений-в-репозиторий

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'Hellio World! ЗАГОТОВКА',
  });
});
// komment
// http://localhost:3000/task2b/?fullname=Steven Paul Jobs
function test_response(ss) {
    let testrez = false;
    console.log(ss.toString());
    // let i=0;
    let elem_ss = 'asdf';
    // for (i=0 ; i<=ss.length ; i++)
    console.log('qwerty ss=');
    console.log(ss);
    for(var i = 0; i < ss.length; i++){
    // for (elem_ss in ss)
      console.log('i=');
      console.log(i);
      elem_ss = ss[i];
      console.log('elem_ss=');
      console.log(elem_ss);

      let poz = elem_ss.search(/[^a-zA-Zа-яА-ЯёЁó']/);
      console.log('poz='+poz);
      if (poz >= 0) {
          return false;
      } else {
          testrez = true;
      };
    };
    return testrez;
}

function fist_up(s) {
  return s.toUpperCase().slice(0,1) + s.toLowerCase().slice(1);
}

app.get('/task2b', (req, res) => {
  let rez = req.query.fullname;
  console.log(rez.toString());
  let ts = rez;
  ts = ts.toString();
  ts = ts.trim();
  // Split String
  let ss = ts.split(/\s+/);
  // console.log(ss);
  // console.log(ss.length);


  let fam = 'фамилия';
  let imya = 'имя';
  let och = 'отчество';
  if (ss.length === 1) {
    if (ss[0] === '') {
      rez = 'Invalid fullname';
    } else {
      ss[0] = fist_up(ss[0]);
      fam = ss[0];
      rez = fam;
    }
  }
  if (ss.length === 2) {
    ss[0] = fist_up(ss[0]);
    ss[1] = fist_up(ss[1]);
    fam = ss[1];
    imya = ss[0];
    rez = '' + fam.toString() + ' ' + imya[0].toString() + '.';
  }
  if (ss.length === 3) {
      ss[0] = fist_up(ss[0]);
      ss[1] = fist_up(ss[1]);
      ss[2] = fist_up(ss[2]);
      fam = ss[2];
      imya = ss[0];
      och = ss[1]
      rez = '' + fam.toString() + ' ' + imya[0].toString() + '. ' + och[0].toString() + '.';
  }
  console.log('вызываем test_response ss=');
  console.log(ss);
  if (ss.length > 3 || ss.length === 0 || !test_response(ss)) {
    rez = 'Invalid fullname';
  }
  // if (test_response(ss)) {
  //   rez = 'Invalid fullname';
  // }
  res.send(rez.toString());
});
// http://localhost:3000/task2A/?a=2&b=82
app.listen(3000, function () {
  console.log('my task2b Example app listening on port 3000!');
});
